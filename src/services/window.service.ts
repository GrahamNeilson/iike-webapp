/**
 * Simply makes the normal, global window object injectable.
 * Aids testing.
 */
import { Injectable } from '@angular/core'

function _window(): any {
	// return the global native browser window object
	return window
}
@Injectable()
export class WindowService {
	public nativeWindow(): any {
		return _window()
	}
}
