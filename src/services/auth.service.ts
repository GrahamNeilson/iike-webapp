// @angular/http for the interface, provider is our own defined in core module, injected in constructor
// use http interceptor?
import { Http } from '@angular/http'
import { Injectable } from '@angular/core'
import { authUrl } from '../app/app.constants'

@Injectable()
export class AuthenticationService {

	constructor(private http: Http) { }

	loginByGoogleAccessCode(code: string): any {
		return this.authorizeByGoogleAccessCode(code, `${authUrl}/login`)
	}

	registerByGoogleAccessCode(code: string): any {
		return this.authorizeByGoogleAccessCode(code, `${authUrl}/register`)
	}

	authorizeByGoogleAccessCode(code, url) {
		let body: any = {
			code,
			redirectUrl: window.location.origin,
		}

		body = JSON.stringify(body)

		/**
     * return an observabe
     */
		return this.http.post(url, body)
			.map(res => res.json())
	}

	verify() {
		/**
     * return an observable
     */
		return this.http.get(`${authUrl}/verify`)
			.map(res => res.json())
	}
}

