import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { Http } from '@angular/http'

import { Observable } from '../app/app.rx'
import * as Actions from '../store/actions'
import { ITask, ITaskCategory, ITaskAsset, IAppStore } from '../store/interfaces'
import { handleResponseError } from './http.helpers'
import { tasksUrl } from '../app/app.constants'

@Injectable()
export class TasksService {

	public taskCategories: Observable<ITaskCategory[]> =
		this._http
			.get(`${tasksUrl}/categories`)
			.map(res => res.json())

	public taskAssets: Observable<ITaskAsset[]> =
		this._http
			.get(`${tasksUrl}/assets`)
			.map(res => res.json())

	constructor(
		private _http: Http,
		private _store: Store<IAppStore>) {
	}

	public createTask(task: any): Observable<void> {
		return this._http
			.post(tasksUrl, JSON.stringify(task))
			.map(res => res.json())
	}

	public editTask(task: any): Observable<void> {
		return this._http
			.post(`${tasksUrl}/edit`, JSON.stringify(task))
			.map(res => res.json())
	}

	public takeOnTask(taskId: number): Observable<void> {
		return this._http
			.post(`${tasksUrl}/takeOn`, JSON.stringify({taskId}))
			.map(res => res.json())
	}

	public reject(taskId: number): Observable<void> {
		return this._http
			.post(`${tasksUrl}/reject`, JSON.stringify({taskId}))
			.map(res => res.json())
	}

	public complete(taskId: number): Observable<void> {
		return this._http
			.post(`${tasksUrl}/complete`, JSON.stringify({taskId}))
			.map(res => res.json())
	}

	public acceptAsComplete(taskId: number): Observable<void> {
		return this._http
			.post(`${tasksUrl}/acceptAsComplete`, JSON.stringify({taskId}))
			.map(res => res.json())
	}

	public rejectAsComplete(taskId: number): Observable<void> {
		return this._http
			.post(`${tasksUrl}/rejectAsComplete`, JSON.stringify({taskId}))
			.map(res => res.json())
	}
}
