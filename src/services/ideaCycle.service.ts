import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { Http } from '@angular/http'

import { Observable } from '../app/app.rx'
import { handleResponseError } from './http.helpers'
import { IAppStore, ITask, IIdeaCycle } from '../store/interfaces'
import * as Actions from '../store/actions'
import { ideaCyclesUrl } from '../app/app.constants'

@Injectable()
export class IdeaCyclesService {
	public ideaCycleTree$: Observable<any>
	public ideaCycle$: Observable<IIdeaCycle>
	public ideaCycleTasks$: Observable<ITask[]>

	constructor(
		private _http: Http,
		private _store: Store<IAppStore>) {

		this.ideaCycle$ = _store.select('ideaCycle')
		this.ideaCycleTree$ = _store.select('ideaCycleTree')
		this.ideaCycleTasks$ = _store.select('ideaCycleTasks')
	}

	public deleteIdeaCycle(): void {
		this._store.dispatch({
			type: Actions.DELETE_IDEA_CYCLE
		})
	}

	public deleteIdeaCycleTree(): void {
		this._store.dispatch({
			type: Actions.DELETE_IDEA_CYCLE_TREE
		})
	}

	public deleteIdeaCycleTasks(): void {
		this._store.dispatch({
			type: Actions.DELETE_IDEA_CYCLE_TASKS
		})
	}

	public loadIdeaCycleTree(id: number): void {
		this._http.get(`${ideaCyclesUrl}/${id}/children`)
			.map(res => res.json())
			.map(payload => ({
				type: Actions.ADD_IDEA_CYCLE_TREE,
				payload,
			}))
			.subscribe(action => this._store.dispatch(action))
	}

	public reactiveIdeaCycle(id: number): Observable<any> {
		return this._http.get(`${ideaCyclesUrl}/${id}/reactivate`)
			.map(res => res.json())
	}

	public loadIdeaCycleTasks(id: number): void {
		this._http.get(`${ideaCyclesUrl}/${id}/tasks`)
			.map(res => res.json())
			.map(payload => ({
				type: Actions.ADD_IDEA_CYCLE_TASKS,
				payload,
			}))
			.subscribe(action => this._store.dispatch(action))
	}

	public createFreshCycle(ideaCycleId: number): Observable<any> {
		return this._http
			.post(`${ideaCyclesUrl}/create-fresh-cycle`, JSON.stringify({
				ideaCycleId,
			}))
			.map(res => res.json())
	}

	public createCycleFromPrevious(ideaCycleId: number): Observable<any> {
		return this._http
			.post(`${ideaCyclesUrl}/create-cycle-from-previous`, JSON.stringify({
				ideaCycleId,
			}))
			.map(res => res.json())
	}
}
