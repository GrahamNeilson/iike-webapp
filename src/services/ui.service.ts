import { Store } from '@ngrx/store'
import { Injectable } from '@angular/core'

import { Observable } from '../app/app.rx'
import { IAppStore } from '../store/interfaces'
import * as Actions from '../store/actions'

@Injectable()
export class UiService {
	public ui: Observable<any>

	constructor(private _store: Store<IAppStore>) {
		this.ui = _store.select('ui')
	}

	public toggleTheme(): void {
		this._store.dispatch({ type: Actions.TOGGLE_THEME, payload: {} })
	}

	public toggleDevtools(): void {
		this.ui.take(1).subscribe(ui => {
			ui.devtoolsOpen ?
				this._store.dispatch({ type: Actions.CLOSE_DEVTOOLS }) :
				this._store.dispatch({ type: Actions.OPEN_DEVTOOLS })
		})
	}

	public toggleTasks(initialTabIndex: number): void {
		this.ui.take(1).subscribe(ui => {
			this._store.dispatch({
				type: Actions.SET_INITIAL_TAB,
				payload: initialTabIndex,
			})

			ui.tasksOpen ?
				this._store.dispatch({ type: Actions.CLOSE_TASKS }) :
				this._store.dispatch({ type: Actions.OPEN_TASKS })
		})
	}
}
