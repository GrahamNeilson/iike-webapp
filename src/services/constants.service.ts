import { Injectable } from '@angular/core'

@Injectable()
export class ConstantsService {
	public idea = {
		title: {
			maxlength: 45,
			minlength: 3,
		},
		description: {
			maxlength: 255,
			minlength: 16,
		}
	}

	public task = {
		title: {
			maxlength: 45,
			minlength: 3,
		},
		description: {
			maxlength: 255,
			minlength: 16,
		}
	}
}
