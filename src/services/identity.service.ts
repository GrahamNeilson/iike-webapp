import { Store } from '@ngrx/store'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'

import { Observable } from '../app/app.rx'
import { IAppStore, IIdentity } from '../store/interfaces'
import { Actions } from '../store'

/**
 * Deals with setting and clearing the current identity
 * Stored in cookie as will be sent with fresh requests in order to bootstrap
 * any previous identity user
 * I suppose this could be moved to an api style request / response on init in future.
 */
@Injectable()
export class IdentityService {
	public identity: Observable<IIdentity>
	/**
   * Used by AuthGuard for quick synchronous client route auth checking.
   */
	public isLoggedIn = false

	constructor(
		private _store: Store<IAppStore>,
		private _router: Router) {

		this.identity = _store.select('identity')
	}

	public login(identity: any, token: string = undefined, url: string): void {
		if (token) {
			this.setAuthToken(token)
		}

		const action = {
			type: Actions.SET_IDENTITY,
			payload: {
				identity,
			},
		}

		this._store.dispatch(action)
		this.isLoggedIn = true
		this._router.navigate([url])
	}

	public logout(): void {
		this.deleteAuthToken()

		const action = {
			type: Actions.DELETE_IDENTITY,
		}

		this._store.dispatch(action)
		this.isLoggedIn = false

		/**
     * always redirect home
     */
		this._router.navigate(['/'])
	}

	public getAuthToken(): string {
		return localStorage.getItem('authToken')
	}

	private setAuthToken(token): void {
		localStorage.setItem('authToken', token)
	}

	private deleteAuthToken(): void {
		localStorage.removeItem('authToken')
	}
}
