import { Injectable } from '@angular/core'
import { CanActivate, CanLoad, Router } from '@angular/router'

import { IdentityService } from './identity.service'

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(private _identityService: IdentityService, private _router: Router) { }

	canActivate() {
		if (this._identityService.isLoggedIn) {
			return true
		}

		this._router.navigate(['/'])
		return false
	}

	canLoad() {
		if (this._identityService.isLoggedIn) {
			return true
		}

		this._router.navigate(['/'])
		return false
	}
}
