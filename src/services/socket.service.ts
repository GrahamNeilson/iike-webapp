import { Store } from '@ngrx/store'
import { Injectable } from '@angular/core'
import * as io from 'socket.io-client'

import { environment } from '../environments/environment'
import { IAppStore } from '../store/interfaces'
import { IdeasService } from './'

@Injectable()
export class SocketService {
	socket: any

	constructor(private _store: Store<IAppStore>) {
		this.socket = io(environment.apiUrl)

		this.socket.on('message', ({type, payload}): any => {
			console.log(type)
			console.log(payload)
			this._store.dispatch({
				type,
				payload,
			})
		})
	}
}
