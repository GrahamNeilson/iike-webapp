import { Store } from '@ngrx/store'
import { Injectable } from '@angular/core'
import { Http } from '@angular/http'

import { Observable } from '../app/app.rx'
import { IAppStore, IUser } from '../store/interfaces'
import { usersUrl } from '../app/app.constants'
import * as Actions from '../store/actions'

@Injectable()
export class UsersService {
	public users: Observable<any>
	public user: Observable<any>
	public selectedUser: Observable<any>

	constructor(private _http: Http, private _store: Store<IAppStore>) {
		this.users = _store.select('users')
	}

	public loadUsers(): void {
		this._http.get(usersUrl)
			.map(res => res.json())
			.map(payload => ({
				type: Actions.ADD_USERS,
				payload,
			}))
			.subscribe(action => this._store.dispatch(action))
	}

	getUsers(): Observable<IUser[]> {
		return this._http.get(usersUrl)
			.map(res => res.json())
	}

	public updateAbilities(id: number, abilities: string[]): void {
		this._http.put(`${usersUrl}/${id}/abilities`, {
			id,
			abilities,
		})
			.map(res => res.json())
			.map(payload => ({
				type: Actions.UPDATE_USER,
				payload,
			}))
			.subscribe(action => this._store.dispatch(action))
	}

	public requestUser(id: number): void {
		this._store.dispatch({
			type: Actions.REQUEST_USER,
			payload: {
				id,
			}
		})
	}
}
