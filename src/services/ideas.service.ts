import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { Http } from '@angular/http'

import { Observable } from '../app/app.rx'
import { handleResponseError } from './http.helpers'
import { IAppStore, IIdea } from '../store/interfaces'
import * as Actions from '../store/actions'
import { ideasUrl } from '../app/app.constants'

@Injectable()
export class IdeasService {
	public idea$: Observable<IIdea>
	constructor(
		private _http: Http,
		private _store: Store<IAppStore>) {

		this.idea$ = _store.select('idea')
	}

	// post should only need report on success / fail. updates via socket
	public createIdea(idea: IIdea): Observable<{}> {
		return this._http
			.post(ideasUrl, JSON.stringify(idea))
			.map(res => res.json())
			.catch(err => {
				return handleResponseError(err)
			})
	}

	public loadRootIdea(): void {
		this._http.get(`${ideasUrl}/root-idea`)
			.map(res => res.json())
			.map(payload => ({
				type: Actions.ADD_IDEA, // causes middleware effect
				payload,
			}))
			.subscribe(action => this._store.dispatch(action))
	}

	public loadIdea(id: number): void {
		this._http.get(`${ideasUrl}/${id}`)
			.map(res => res.json())
			.map(payload => ({
				type: Actions.ADD_IDEA, // causes middleware effect
				payload,
			}))
			.subscribe(action => this._store.dispatch(action))
	}

	public ideaPath$ = (id: number): Observable<IIdea[]> =>
		this._http
			.get(`${ideasUrl}/${id}/path`)
			.map(res => res.json())

	public deleteIdea(): void {
		this._store.dispatch({
			type: Actions.DELETE_IDEA
		})
	}
}
