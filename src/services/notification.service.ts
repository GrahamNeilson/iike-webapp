import { Injectable, Component } from '@angular/core'
import { MatSnackBar } from '@angular/material'

@Injectable()
export class NotificationService {
	constructor(public snackBar: MatSnackBar) { }

	public showSuccess(message: string): void {
		this._show(message, ['notification-success'])
	}

	public showError(message: string): void {
		this._show(message, ['notification-error'])
	}

	private _show(message: string, extraClasses: string[] = []): void {
		const snackBarRef = this.snackBar.open(message, ' Got it ', {
			duration: 30000,
			extraClasses,
		})
	}
}
