import { Response } from '@angular/http'

import { Observable } from '../app/app.rx'

/**
 * this might be better as a service, or as part of part of http interceptor?
 * think about that, see how current feels.
 * service would be better for testing, so service.
 * SERVICE, with static methods.
 * would prob be simpler in http module in terms of imports
 */

/**
 * helper to pull out the res json data
 * we don't really need to put the data in a data property do we? - this is
 * from the angular docs, perhaps just showing what you would do with that format.
 */
export function extractResponseData(res: Response): any {
	const body = res.json()
	return body || {}
}

/**
 * helper to deal with http errors
 */
export function handleResponseError(error: any) {
	// In a real world app, we might use a remote logging infrastructure
	// We'd also dig deeper into the error to get a better message
	const errMsg = (error.message) ? error.message :
		error.status ? `${error.status} - ${error.statusText}` : 'Server error'
	console.error(errMsg) // log to console instead
	return Observable.throw(errMsg)
}
