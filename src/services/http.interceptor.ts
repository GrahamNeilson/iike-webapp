import { Router } from '@angular/router'
import {
	Http,
	Request,
	RequestOptionsArgs,
	Response,
	RequestOptions,
	ConnectionBackend,
	Headers,
} from '@angular/http'

import { Observable } from '../app/app.rx'
import { IdentityService } from './identity.service'

/**
 * https://www.illucit.com/blog/2016/03/angular2-http-authentication-interceptor/
 */
export class HttpInterceptor extends Http {

	constructor(
		backend: ConnectionBackend,
		defaultOptions: RequestOptions,
		private _router: Router,
		private _identityService: IdentityService) {
		super(backend, defaultOptions)
	}

	request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
		return this.intercept(super.request(url, options))
	}

	get(url: string, options?: RequestOptionsArgs): Observable<Response> {
		return this.intercept(super.get(url, this.getRequestOptionArgs(options)))
	}

	post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
		return this.intercept(super.post(url, body, this.getRequestOptionArgs(options)))
	}

	put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
		return this.intercept(super.put(url, body, this.getRequestOptionArgs(options)))
	}

	delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
		return this.intercept(super.delete(url, this.getRequestOptionArgs(options)))
	}

	private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
		if (options == null) {
			options = new RequestOptions()
		}
		if (options.headers == null) {
			options.headers = new Headers()
		}

		options.headers.append('Content-Type', 'application/json')

		const token = this._identityService.getAuthToken()
		if (token) {
			options.headers.append('Authorization', `Bearer ${token}`)
		}

		return options
	}

	private intercept(observable: Observable<Response>) {
		return observable
			.map((response: Response) => {
				/**
         * ok
         */
				return response
			})
			.catch((error, source) => {
				if (error.status === 500) {
					// unexpected errors.
					return Observable.throw(error)
					// this._router.navigate(['/error', { error }])
					// return Observable.empty()
				} else if (error.status === 401) {
					this._identityService.logout()
					this._router.navigate(['/'])
					// an observable empty completes.
					return Observable.empty()
				} else if (error.status === 403) {

					this._identityService.logout()
					this._router.navigate(['/'])
					return Observable.empty()

				} else {
					return Observable.throw(error)
				}
			})
	}
}
