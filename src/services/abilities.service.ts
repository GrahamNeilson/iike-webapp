import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { Http } from '@angular/http'

import { Observable } from '../app/app.rx'
import { extractResponseData, handleResponseError } from './http.helpers'
import { IAppStore, IAbility, IUser } from '../store/interfaces'
import * as Actions from '../store/actions'
import { abilitiesUrl } from '../app/app.constants'

@Injectable()
export class AbilitiesService {
	abilities: Observable<any>
	selectedAbility: Observable<any>
	abilityUsers: Observable<any>

	constructor(
		private _http: Http,
		private _store: Store<IAppStore>) {

		this.abilities = _store.select('abilities')
		this.selectedAbility = _store.select('selectedAbility')

		this.abilityUsers = this.createAbilityUsers()
	}

	/*
   * Create an observable from from the selected ability observable,
   * a la the search box examples (i.e. go through that again)
   */
	createAbilityUsers(): Observable<IUser[]> {
		/*
     * flat map once as we have a depth of 2:
     * we map over the selected ability
     * we then get an observable back from http call
     */
		return this.selectedAbility.
			mergeMap(ability => {
				return this.loadAbilityUsers(ability)
			})
	}

	// Retrieve all users posessing the given ability
	loadAbilityUsers(ability: IAbility): Observable<IUser[]> {
		if (!ability.id) {
			return Observable.from([[]])
		}

		return this._http.
			get(`${abilitiesUrl}/${ability.id}/users`)
			.map(res => {
				return res.json()
			})
	}

	// get all abilities and pass to the store, used typically on init of
	// main ability component
	loadAbilitiesToStore(): void {
		this._http.get(abilitiesUrl)
			.map(res => res.json())
			.map(payload => ({
				type: Actions.ADD_ABILITIES,
				payload,
			}))
			.subscribe(action => this._store.dispatch(action))
	}

	// test, for on the fly usage without a store
	loadAbilities(): Observable<IAbility[]> {
		return this._http.get(abilitiesUrl)
			.map(res => res.json())
	}


	// update the store with the selected ability
	selectAbility(ability: IAbility): IAbility {
		this._store.dispatch({
			type: Actions.SELECT_ABILITY,
			payload: {
				ability,
			},
		})
		// for chaining
		return ability
	}

	// Update the store with no selected ability
	deselectAbility(): void {
		this._store.dispatch({
			type: Actions.DESELECT_ABILITY,
		})
	}

	createAbility(ability: IAbility): Observable<{}> {
		const { title } = ability
		const data = JSON.stringify({ title })

		return this._http.post(abilitiesUrl, data)
			.map(extractResponseData)
			.map(a => this.addAbilityToStore(a))
			.map(a => this.selectAbility(a))
			.catch(handleResponseError)
	}

	updateAbility(ability: IAbility): Observable<{}> {
		const { id, title } = ability
		const data = JSON.stringify({
			id,
			title,
		})

		return this._http.put(`${abilitiesUrl}/${id}`, data)
			.map(extractResponseData)
			.map(ability => this.updateAbilityInStore(ability))
			.map(ability => this.selectAbility(ability))
			.catch(handleResponseError)
	}

	deleteAbility(id: number): Observable<{}> {
		return this._http.delete(`${abilitiesUrl}/${id}`)
			.map(() => this.deleteAbilityFromStore(id))
			.map(() => this.deselectAbility())
			.catch(handleResponseError)
	}

	// add to store, return the ability for chaining
	addAbilityToStore(ability: IAbility): IAbility {
		this._store.dispatch({
			type: Actions.ADD_ABILITY,
			payload: ability,
		})

		return ability
	}

	// update store, return the ability for chaining
	updateAbilityInStore(ability: IAbility): IAbility {
		this._store.dispatch({
			type: Actions.UPDATE_ABILITY,
			payload: ability,
		})

		return ability
	}

	deleteAbilityFromStore(id: number): void {
		this._store.dispatch({
			type: Actions.DELETE_ABILITY,
			payload: id,
		})
	}
}
