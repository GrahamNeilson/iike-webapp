import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Observable'
import { Http } from '@angular/http'
import { Store } from '@ngrx/store'
// the name clashes with the name we give to namespace our web app reducer actions,
// so use an alias.
import { Actions as NgRxActionStream, Effect } from '@ngrx/effects'

import * as Actions from '../actions'
import { ideasUrl } from '../../app/app.constants'
import { IAppStore } from '../../store/interfaces'

@Injectable()
export class IdeaEffects {

	// when we load an idea, load the current idea cycle
	@Effect() loadCurrentIdeaCycle$ = this.actions$
		.ofType(Actions.ADD_IDEA)
		.switchMap((action: any) => {
			return this._http.get(`${ideasUrl}/${action.payload.id}/current-cycle`)
				.map(res => res.json())
				.map(payload => ({
					type: Actions.ADD_IDEA_CYCLE,
					payload,
				}))
		})

	constructor(
		private actions$: NgRxActionStream,
		private _http: Http,
		private _store: Store<IAppStore>) { }
}
