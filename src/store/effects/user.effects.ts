import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Observable'
import { Http } from '@angular/http'
import { Store } from '@ngrx/store'
// the name clashes with the name we give to namespace our web app reducer actions,
// so use an alias.
import { Actions as NgRxActionStream, Effect } from '@ngrx/effects'

import * as Actions from '../actions'
import { usersUrl } from '../../app/app.constants'
import { IAppStore } from '../../store/interfaces'

@Injectable()
export class UserEffects {

	@Effect() requestUser$ = this.actions$
		.ofType(Actions.REQUEST_USER)
		.switchMap((action: any) => {
			const id = action.payload.id

			return this._http.get(`${usersUrl}/${id}`)
				.map(res => res.json())
				.map(payload => {
					return {
						type: Actions.REQUEST_USER_FULFILLED,
						payload,
					}
				})
		})

	constructor(
		private actions$: NgRxActionStream,
		private _http: Http,
		private _store: Store<IAppStore>) { }
}
