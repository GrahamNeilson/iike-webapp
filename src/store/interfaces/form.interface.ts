import { Observable } from '../../app/app.rx'
import { IIdea } from './'

// Types that can be used in the generic form dialog
export type IFormSubject = IIdea

export interface IResult {
	success: boolean
	formSubject: IFormSubject
}

export type ISaveFunction = (formSubject: IFormSubject) => Observable<IResult>
