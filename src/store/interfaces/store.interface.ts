import {
	IAbility,
	IIdea,
	IIdeaCycle,
	ITask,
	IIdentity,
	IUi,
	IUser,
} from './'

export interface IAppStore {
	abilities: IAbility[]
	ideas: IIdea[]
	idea: IIdea
	ideaCycle: IIdeaCycle
	ideaCycleTree: IIdeaCycle[]
	ideaCycleTasks: ITask[]
	ideaTree: IIdea[]
	identity: IIdentity
	selectedAbility: IAbility
	ui: IUi
	users: IUser[]
}
