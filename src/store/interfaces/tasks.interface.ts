import { IUser } from './index'

export interface ITask {
	id: number
	folder_id?: string
	title?: string
	description?: string
	idea_cycle_id?: number
	task_category_id?: number
	task_asset_id?: number
	is_default?: boolean
	created_by?: number
	created_date?: Date
	accepted?: boolean
	accepted_by?: number
	acepted_date?: Date
	completed?: boolean
	completed_by?: number
	completed_date?: Date
	assigned_date?: Date
	task_category_title?: string
	task_asset_title?: string
	cycle?: number
	idea_title?: string
	user_id?: number
	users?: ITaskUser[]
	user_display_name?: string
}

export interface ITaskUser extends ITask {
	user_id: number
	permission_id?: string
	assigned_date?: Date
	taken_on?: boolean
	taken_on_date?: Date
	picture_url?: string
	display_name: string
	google_id?: string,
	user_display_name?: string
}

export interface ITaskCategory {
	id: number
	title: string
	isDefault: boolean
}

export interface ITaskAsset {
	id: number
	title: string
	isDefault: boolean
}
