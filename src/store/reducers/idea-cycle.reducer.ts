import { ActionReducer, Action } from '@ngrx/store'

import {IAction, ITask} from '../interfaces'
import * as Actions from '../actions'

export interface IIdeaCycle {
	id: number
	idea_id: number
	created_date: Date
	date_entered: Date
	date_completed: Date
	cycle: number
	folder_id: string
	in_progress: Boolean
	active: Boolean
	idea_title: string
	idea_lft: number
	idea_rgt: number
	user_display_name: string
	user_picture_url: string
}

export function IdeaCycleReducer
	(state: IIdeaCycle = undefined, { type, payload }: IAction): IIdeaCycle {
		switch (type) {
			case Actions.ADD_IDEA_CYCLE:
				return payload

			case Actions.DELETE_IDEA_CYCLE:
				return undefined

			case Actions.IDEA_CYCLE_UPDATED:
				return (payload.id === state.id) ? payload : state

			case Actions.ADDITIONAL_IDEA_CYCLE_CREATED:
				return (payload.idea_id === state.idea_id) ? payload : state

			default:
				return state
		}
	}
