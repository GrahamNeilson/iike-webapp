import { ActionReducer, Action } from '@ngrx/store'

import * as Actions from '../actions'
import {IAction} from '../interfaces'

export interface IUi {
	devtoolsOpen: boolean
	darkTheme: boolean
	tasksOpen: boolean
	tasksTabIndex: number
}

const initialState: IUi = {
	devtoolsOpen: false,
	darkTheme: true,
	tasksOpen: false,
	tasksTabIndex: 0,
}

export function UiReducer
	(state = initialState, { type, payload }: IAction): IUi {
		switch (type) {
			case Actions.OPEN_DEVTOOLS:
				return Object.assign({}, state, {
					devtoolsOpen: true,
				})

			case Actions.CLOSE_DEVTOOLS:
				return Object.assign({}, state, {
					devtoolsOpen: false,
				})

			case Actions.TOGGLE_THEME:
				return Object.assign({}, state, {
					darkTheme: !state.darkTheme,
				})

			case Actions.OPEN_TASKS:
				return Object.assign({}, state, {
					tasksOpen: true,
				})

			case Actions.CLOSE_TASKS:
				return Object.assign({}, state, {
					tasksOpen: false,
				})

			case Actions.SET_INITIAL_TAB:
				return Object.assign({}, state, {
					tasksTabIndex: payload,
				})

			default:
				return state
		}
	}
