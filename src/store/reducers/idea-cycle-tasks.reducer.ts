import { ActionReducer, Action } from '@ngrx/store'

import { ITask, IAction } from '../interfaces'
import * as Actions from '../actions'

export function IdeaCycleTasksReducer
	(state: ITask[] = [], { type, payload }: IAction): ITask[] {

		switch (type) {
			case Actions.ADD_IDEA_CYCLE_TASKS:
				return payload

			case Actions.DELETE_IDEA_CYCLE_TASKS:
				return []

			case Actions.TASK_TAKEN_ON:
			case Actions.TASK_REJECTED_AS_COMPLETE:
			case Actions.TASK_ACCEPTED_AS_COMPLETE:
			case Actions.TASK_REJECTED:
			case Actions.TASK_COMPLETED:
			case Actions.TASK_EDITED:
				return state.map(task => {
					return (task.id === payload.id) ? payload : task
				})

			case Actions.NEW_TASK:
				// as there will always be at least the default tasks if a cycle loaded, we can use the first
				// one to check the cylcle id of any new socket task added.
				const task: ITask = payload

				if (state[0] && state[0].idea_cycle_id === task.idea_cycle_id) {
					return [...state, payload]
				}

				return state

			default:
				return state
		}
	}
