import { ActionReducer, Action } from '@ngrx/store'

import * as Actions from '../actions'
import {ITask, IAction} from '../interfaces'

export interface IUser {
	id: number
	name: string
	display_name: string
	tasks: ITask[]
	loading?: boolean // hack - todo, remove
	first_name: string
	surname: string
	email: string
}

const initialState: IUser[] = []

export function UsersReducer
	(state = initialState, { type, payload }: IAction): IUser[] {
		switch (type) {
			case Actions.ADD_USERS:
				return payload

			case Actions.UPDATE_USER:
				return state.map(user => {
					if (user.id === payload.id) {
						return payload
					}

					return user
				})

			default:
				return state
		}
	}

export interface IRequestedUser {
	loading: boolean
	id?: number
}

const initialUserState: IRequestedUser = {
	loading: false,
}

export function UserReducer
	(state: IRequestedUser, { type, payload }: any): IRequestedUser {
		switch (type) {
			case Actions.REQUEST_USER:
				return Object.assign({}, initialUserState, { loading: true })

			case Actions.DESELECT_USER:
				return Object.assign({}, initialUserState)

			case Actions.REQUEST_USER_FULFILLED:
				return Object.assign({}, initialUserState, ...payload)

			default:
				return state
		}
	}
