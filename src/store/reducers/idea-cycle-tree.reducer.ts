import { ActionReducer, Action } from '@ngrx/store'

import { IIdeaCycle, IAction } from '../interfaces'
import * as Actions from '../actions'

export function IdeaCycleTreeReducer
	(state: IIdeaCycle[] = [], { type, payload }: IAction): IIdeaCycle[] {
		switch (type) {
			case Actions.ADD_IDEA_CYCLE_TREE:
				return payload

			case Actions.DELETE_IDEA_CYCLE_TREE:
				return []

			case Actions.NEW_IDEA:
				if (state[0].idea_id === payload.parent_idea_id) {
					return [...state, payload.idea_cycle]
				}

				return state

			case Actions.IDEA_CYCLE_UPDATED:
				return [...state.map(ideaCycle =>
					ideaCycle.id === payload.id ? payload : ideaCycle
				)]

			default:
				return state
		}
	}
