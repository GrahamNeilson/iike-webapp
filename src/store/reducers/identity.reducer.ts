import { Action, ActionReducer} from '@ngrx/store'

import * as Actions from '../actions'
import {IAction, ITask} from '../interfaces'

export interface IIdentity {
	id: number
	tasks?: ITask[]
}

const initialState: IIdentity = {
	id: 0,
	tasks: []
}

export function IdentityReducer
	(state = initialState, {type, payload}: IAction): IIdentity {

		switch (type) {
			case Actions.SET_IDENTITY:
				return Object.assign({}, ...payload.identity)

			case Actions.DELETE_IDENTITY:
				return initialState

			case Actions.NEW_IDEA:
				const ideaTasks = payload.idea_cycle.tasks

				return Object.assign(state, {
					// tslint:disable-next-line:no-shadowed-variable
					tasks: ideaTasks.reduce((accumulatedTasks, task) => {
						// if the identity user is an assignee, add to user tasks
						if (task.users.filter(u => u.user_id === state.id).length) {
							return [...accumulatedTasks, task]
						}
						return accumulatedTasks
					}, state.tasks)
				})

				case Actions.ADDITIONAL_IDEA_CYCLE_CREATED:
					// as new idea, but payload is an idea cycle, not an idea
					const ideaCycleTasks = payload.tasks

					return Object.assign(state, {
						// tslint:disable-next-line:no-shadowed-variable
						tasks: ideaCycleTasks.reduce((accumulatedTasks, task) => {
							// if the identity user is an assignee, add to user tasks
							if (task.users.filter(u => u.user_id === state.id).length) {
								return [...accumulatedTasks, task]
							}
							return accumulatedTasks
						}, state.tasks)
					})

			case Actions.TASK_TAKEN_ON:
			case Actions.TASK_REJECTED_AS_COMPLETE:
			case Actions.TASK_ACCEPTED_AS_COMPLETE:
			case Actions.TASK_COMPLETED:
				return Object.assign({}, state, {
					tasks: state.tasks.map(task =>
						task.id === payload.id ? payload : task
					)
				})

			case Actions.TASK_EDITED:
				const taskWasAssigned = state.tasks.filter(t => t.id === payload.id).length > 0
				const taskIsAssigned = payload.users.map(user => user.user_id).indexOf(state.id) !== -1

				if (taskIsAssigned && !taskWasAssigned) {
					return Object.assign(state, {
						tasks: [...state.tasks, payload]
					})
				}

				if (!taskIsAssigned && state) {
					return Object.assign(state, {
						tasks: state.tasks.filter(t => t.id !== payload.id)
					})
				}

				return Object.assign({}, state, {
					tasks: state.tasks.map(task =>
						task.id === payload.id ? payload : task
					)
				})

			case Actions.TASK_REJECTED:
				return Object.assign({}, state, {
					tasks: state.tasks.filter(task =>
						task.id !== payload.id
					)
				})

			case Actions.NEW_TASK:
				const task = payload
				const taskIsAssignedToIdentity = task.users.filter(u => u.user_id === state.id).length

				return taskIsAssignedToIdentity ?
					Object.assign({}, state, {
						tasks: [...state.tasks, payload]
					}) : state

			default:
				return state
		}
	}
