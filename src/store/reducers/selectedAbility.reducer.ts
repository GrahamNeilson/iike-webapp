import { Action, ActionReducer } from '@ngrx/store'

import { IAbility, IAction } from '../interfaces'
import * as Actions from '../actions'

const initialState: IAbility = {
	id: undefined,
	title: undefined,
}

export function SelectedAbilityReducer
	(state = initialState, { type, payload }: IAction): IAbility {
		switch (type) {
			case Actions.SELECT_ABILITY:
				return Object.assign({}, ...payload.ability)

			case Actions.DESELECT_ABILITY:
				return {
					id: undefined,
					title: undefined,

				}

			default:
				return state
		}
	}
