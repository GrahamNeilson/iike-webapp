import { ActionReducer, Action } from '@ngrx/store'

import {IAction} from '../interfaces'
import * as Actions from '../actions'

export interface IIdea {
	id: number
	title: string
	description: string
	created_date: Date
	active: number
	user_id: number
	user_picture_url: string
}

export function IdeaReducer
	(state: IIdea = undefined, { type, payload}: IAction): IIdea {
		switch (type) {
			case Actions.ADD_IDEA:
				return payload

			case Actions.DELETE_IDEA:
				return undefined

			default:
				return state
		}
	}
