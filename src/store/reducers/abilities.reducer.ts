import { ActionReducer, Action } from '@ngrx/store'

import {IAction} from '../interfaces'
import * as Actions from '../actions'

export interface IAbility {
	id: number
	title: string
}

export function AbilitiesReducer
	(state: IAbility[] = [], { type, payload }: IAction): IAbility[] {
		switch (type) {
			case Actions.ADD_ABILITIES:
				return payload

			case Actions.ADD_ABILITY:
				return [...state, payload]

			case Actions.DELETE_ABILITY:
				return state.filter(ability => ability.id !== payload)

			case Actions.UPDATE_ABILITY:
				return state.map(ability => {
					return (ability.id === payload.id) ? payload : ability
				})

			default:
				return state
		}
	}
