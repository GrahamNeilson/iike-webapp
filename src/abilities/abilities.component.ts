import { Component, OnInit, OnDestroy } from '@angular/core'

import { Observable } from '../app/app.rx'
import { IAbility, IUser } from '../store/interfaces'
import { AbilitiesService, NotificationService } from '../services'

@Component({
	selector: 'app-abilities',
	styleUrls: ['./abilities.component.scss'],
	templateUrl: './abilities.component.html',
})
export class AbilitiesComponent implements OnInit, OnDestroy {

	abilities: Observable<Array<IAbility>>
	abilityUsers: Observable<Array<IUser>>
	selectedAbility: Observable<IAbility>

	constructor(
		private _abilitiesService: AbilitiesService,
		private _notificationService: NotificationService) { }

	ngOnInit() {
		/*
     * Reference some observables from the service.
     */
		this.abilities = this._abilitiesService.abilities
		this.selectedAbility = this._abilitiesService.selectedAbility
		this.abilityUsers = this._abilitiesService.abilityUsers

		/*
     * Kick off a request to populate the store
     */
		this._abilitiesService.loadAbilitiesToStore()
	}

	ngOnDestroy() {
		this._abilitiesService.deselectAbility()
	}

	handleSelected(ability: IAbility) {
		this._abilitiesService.selectAbility(ability)
	}

	handleSaveAbility(ability: IAbility): void {
		ability.id ? this.updateAbility(ability) : this.createAbility(ability)
	}

	createAbility(ability: IAbility) {
		this._abilitiesService.createAbility(ability).subscribe(
			(ability: IAbility) => this._notificationService.showSuccess(`Ability ${ability.title} created`),
			(err: any) => this._notificationService.showError(`Error: ability could not be created`)
		)
	}

	updateAbility(ability: IAbility) {
		this._abilitiesService.updateAbility(ability).subscribe(
			(ability: IAbility) => this._notificationService.showSuccess(`Ability updated`),
			(err: any) => this._notificationService.showError(`Error: ability could not be updated`)
		)
	}

	handleDeleteAbility(ability: IAbility) {
		const id = ability.id

		this._abilitiesService.deleteAbility(id).subscribe(
			(ability: IAbility) => this._notificationService.showSuccess(`Ability deleted`),
			(err: any) => this._notificationService.showError(`Error deleting ability`)
		)
	}

	handleMergeAbility(ability: IAbility) {
		alert(JSON.stringify(ability))
	}

	handleCancel() {
		this._abilitiesService.deselectAbility()
	}
}
