import { Component, Input, Output, EventEmitter } from '@angular/core'

import { IAbility, IUser } from '../store/interfaces'

@Component({
	selector: 'app-add-edit-ability',
	templateUrl: './add-edit-ability.component.html',
})
export class AddEditAbilityComponent {
	internalAbility: IAbility
	originalAbility: IAbility
	existingAbilityTitles: string[]

	@Input()
	abilityUsers: IUser[]

	@Input()
	set editableAbility(editableAbility: IAbility) {
		this.originalAbility = editableAbility
		this.internalAbility = Object.assign({}, editableAbility)
	}

	@Input()
	set abilities(abilities: IAbility[]) {
		this.existingAbilityTitles =
			abilities
				.map(ability => ability.title.toLowerCase())
	}

	@Output()
	saveAbility = new EventEmitter()
	@Output()
	cancel = new EventEmitter()
	@Output()
	deleteAbility = new EventEmitter()
	@Output()
	mergeAbility = new EventEmitter()
}
