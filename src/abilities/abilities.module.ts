import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { SharedModule } from '../shared/shared.module'
import { AbilitiesRoutingModule } from './abilities-routing.module'
import { AbilitiesComponent } from './abilities.component'
import { AbilityListComponent } from './ability-list.component'
import { AbilityUsersComponent } from './ability-users.component'
import { AddEditAbilityComponent } from './add-edit-ability.component'

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		AbilitiesRoutingModule,
	],
	declarations: [
		AbilitiesComponent,
		AbilityListComponent,
		AbilityUsersComponent,
		AddEditAbilityComponent,
	],
	exports: [],
	providers: [],
})
export class AbilitiesModule { }
