import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AbilitiesComponent } from './abilities.component'

@NgModule({
	imports: [RouterModule.forChild([
		{ path: '', component: AbilitiesComponent }
	])],
	exports: [RouterModule]
})
export class AbilitiesRoutingModule { }
