import { Component, Input } from '@angular/core'

import { IUser } from '../store/interfaces'

/**
 * When styling material components that I assume will sort themselves out,
 * do it the top level, across the board, and apply to those components from there.
 * Easier to remove.
 */
@Component({
	selector: 'app-ability-users',
	template: `
    <mat-list>
      <mat-list-item
        *ngFor="let user of abilityUsers" >
        Test
        {{user.display_name}}
      </mat-list-item>
    </mat-list>
  `,
})
export class AbilityUsersComponent {
	@Input() abilityUsers: IUser[]
}
