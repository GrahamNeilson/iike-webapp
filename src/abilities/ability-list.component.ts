import { Component, Input, Output, EventEmitter } from '@angular/core'

import { IAbility } from '../store/interfaces'

@Component({
	selector: 'app-ability-list',
	template: `
    <mat-list>
      <mat-list-item
        *ngFor="let ability of abilities"
        (click)="selected.emit(ability)">
        {{ability.title}}
        <span appFlexFill></span>
        <span *ngIf="ability.user_count > 0">
          {{ability.user_count  }} {{ability.user_count > 1 ? "people" : "person"  }}
        </span>
      </mat-list-item>
    </mat-list>
  `,
})
export class AbilityListComponent {
	@Input() abilities: IAbility[]
	@Output() selected = new EventEmitter()
}
