import { NgModule } from '@angular/core'

import {
	MatAutocompleteModule,
	MatButtonModule,
	MatCardModule,
	MatChipsModule,
	MatDialogModule,
	MatIconModule,
	MatInputModule,
	MatListModule,
	MatMenuModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatSidenavModule,
	MatSnackBarModule,
	MatTabsModule,
	MatToolbarModule,
} from '@angular/material'

@NgModule({
	exports: [
		MatAutocompleteModule,
		MatButtonModule,
		MatCardModule,
		MatChipsModule,
		MatDialogModule,
		MatIconModule,
		MatInputModule,
		MatListModule,
		MatMenuModule,
		MatProgressSpinnerModule,
		MatRadioModule,
		MatSidenavModule,
		MatSnackBarModule,
		MatTabsModule,
		MatToolbarModule,
	],
})
export class MaterialModule { }
