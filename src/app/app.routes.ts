import { ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { HomeComponent } from './home/home.component'
import { AuthGuard } from '../services'

export const routes: Routes = [
	{
		path: '',
		component: HomeComponent,
	}, {
		path: 'abilities',
		loadChildren: '../abilities/abilities.module#AbilitiesModule',
		canLoad: [AuthGuard],
	}, {
		path: 'ideas',
		loadChildren: '../ideas/ideas.module#IdeasModule',
		canLoad: [AuthGuard],
	}, {
		path: 'users',
		loadChildren: '../users/users.module#UsersModule',
	},
]

export const routing: ModuleWithProviders = RouterModule.forRoot(routes)
