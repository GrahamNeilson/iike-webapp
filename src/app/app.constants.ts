import { environment } from '../environments/environment'

/**
 * Move this to a constants service.
 */

const apiUrl = environment.apiUrl

const abilitiesUrl = `${apiUrl}/abilities`
const authUrl = `${apiUrl}/auth`
const usersUrl = `${apiUrl}/users`
const ideasUrl = `${apiUrl}/ideas`
const ideaCyclesUrl = `${apiUrl}/ideaCycles`
const tasksUrl = `${apiUrl}/tasks`

export {
	abilitiesUrl,
	authUrl,
	usersUrl,
	ideasUrl,
	ideaCyclesUrl,
	tasksUrl,
}
