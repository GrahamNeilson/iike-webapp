import { Component, OnInit, OnDestroy } from '@angular/core'

import {
	AuthenticationService,
	IdentityService,
	NotificationService
} from '../../services'

@Component({
	selector: 'app-google-auth',
	templateUrl: './googleOAuth2.0.component.html',
})
export class GoogleOAuth20Component implements OnInit, OnDestroy {
	/*
   * Note / future: Listener should be Rx based?
   */
	private _loginListener
	private _hasListener = false

	initialized = false

	constructor(
		private _authService: AuthenticationService,
		private _identityService: IdentityService,
		private _notificationService: NotificationService) { }

	ngOnInit() {
		/*
     * so this also runs in the pop up loaded page, on return from google login / auth,
     * which returs to window.location.origin (via redirect_uri)
     * it does the postMessage'ing back to registered event listener
     * so wherever we pop the login or register component, it should work fine
     */

		// we don't want the ? from the query string whe accepting the params
		const params = window.location.search.substring(1)

		if (params && window.opener && window.opener.location.origin === window.location.origin) {
			const kvp = params.split('=')
			const code = decodeURIComponent(kvp[1])
			window.opener.postMessage(code, window.location.origin)
		}
	}

	ngOnDestroy() {
		if (this._hasListener) {
			this.removeListener()
		}
	}

	removeListener() {
		window.removeEventListener('message', this._loginListener)
		this._hasListener = false
	}

	handleClick(register = false): void {
		if (!this._hasListener) {
			this._hasListener = true

			/*
       * see forming the Url at:
       * https://developers.google.com/identity/protocols/OAuth2UserAgent
       */
			const clientId = '372878731514-a0q586oh5dn2jc7ia2bf4joon2n6cdb6.apps.googleusercontent.com'

			const urlBuilder = []
			urlBuilder.push(
				'response_type=code',
				`client_id=${clientId}`,
				`redirect_uri=${window.location.origin}`,
				`scope=profile email`,
			)

			const url = `https://accounts.google.com/o/oauth2/auth?${urlBuilder.join('&')}`
			const left = (window.outerWidth - 500) / 2
			const top = (window.outerHeight - 500) / 2.5
			const options = `width=500,height=500,left=${left},top=${top}`
			const popup = window.open(url, 'Title', options)

			window.focus()

			this._loginListener = (event) => {
				this.removeListener()
				if (event.origin === window.location.origin) {
					popup.close()
					this._authorize(event.data, register)
				}
			}

			window.addEventListener('message', this._loginListener)
		}
	}

	private _authorize(code: string, register: boolean) {
		const authorizationObservable = register ?
			this._authService.registerByGoogleAccessCode(code) :
			this._authService.loginByGoogleAccessCode(code)

		/**
     * authorizationObservable is the result of logging in or registering via
     * an http call with a google access code.
     */
		authorizationObservable.subscribe(
			res => {
				const message = register ? 'Registration successfull' : 'Login successfull'
				this._identityService.login(res.user, res.token, '')
				this._notificationService.showSuccess(message)
			},
			err => {
				const message = register ? 'Registration failed' : 'Login failed'
				this._notificationService.showError(message)
				return null
			}
		)
	}
}
