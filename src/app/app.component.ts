import { Component, OnInit, OnChanges } from '@angular/core'
import { Router } from '@angular/router'

import {
	AuthenticationService,
	IdentityService,
	SocketService,
} from '../services'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
})
export class AppComponent implements OnInit, OnChanges {

	identity = null
	initialized = false
	eventsSubscription
	entryPath = ''

	constructor(
		private _authService: AuthenticationService,
		private _identityService: IdentityService,
		private _socketService: SocketService,

		private _router: Router) {
		// get current route, for which there must be a better way...
		this.eventsSubscription =
			this._router.events
				.take(1)
				.subscribe(e => {
					this.entryPath = '/' // broke since 2 -> 4 upgrade was: e.url
				})
	}

	ngOnInit() {
		const authToken = this._identityService.getAuthToken()
		this.identity = this._identityService.identity

		if (authToken) {
			this.verifyAuthToken(authToken)
		} else {
			this.initialized = true
		}
	}

	ngOnChanges() {
		// this.identity = this._identityService.identity
	}

	verifyAuthToken(authToken) {
		this._authService.verify()
			.subscribe(
			user => {
				this._identityService.login(user, authToken, this.entryPath)
			},
			err => {
				// we do not get an error, as a 401 is handled by the http intercetor,
				// which will, for now , perform a clean up logout, and redirect home
			},
			() => {
				// ...so this also means we get a completed, rather than an error, so set completed
				// flag here
				this.initialized = true

				// do we need this follwing a take(1)?
				this.eventsSubscription.unsubscribe()
			}
			)
	}
}
