import { Component } from '@angular/core'

import { Observable } from '../../app/app.rx'
import { IIdentity } from '../../store/interfaces'
import { IdentityService } from '../../services/identity.service'

@Component({
	selector: 'app-home',
	styleUrls: ['./home.component.scss'],
	templateUrl: './home.component.html',
})
export class HomeComponent {
	identity: Observable<IIdentity>

	constructor(private _identityService: IdentityService) {
		this.identity = this._identityService.identity
	}
}
