import { Component } from '@angular/core'

@Component({
	selector: 'app-dashboard',
	template: `
    <div appPadding>
      <mat-card>
        <mat-card-subtitle>Data summaries, visualizations, etc</mat-card-subtitle>
        <mat-card-title>Dashboard</mat-card-title>
        <mat-card-content>
          <p>This is some supporting text.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          tempor incididunt ut labor§e et dolore magna aliqua. Ut enim ad</p>
        </mat-card-content>
      </mat-card>
    </div>
  `,
})
export class DashboardComponent { }
