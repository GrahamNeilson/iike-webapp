import { Component, Input } from '@angular/core'
import { MatSnackBar } from '@angular/material'

import { Observable } from '../app.rx'
import { IUi, IIdentity } from '../../store/interfaces'
import { UiService } from '../../services'


/**
 * mat-sidenav-layout 70 width to take into account devtools.
 */
@Component({
	selector: 'app-layout',
	styleUrls: ['layout.component.scss'],
	templateUrl: 'layout.component.html',
})
export class LayoutComponent {
	@Input()
	identity: IIdentity

	ui: Observable<IUi>

	constructor(private _uiService: UiService) {
		this.ui = this._uiService.ui
	}

	handleCloseTasks() {
		this._uiService.toggleTasks(0)
	}
}
