import { Component, Input, OnChanges } from '@angular/core'

import { UiService } from '../../services'
import { IIdentity } from '../../store/reducers/identity.reducer'
import { ITask } from '../../store/interfaces/index';

@Component({
	selector: 'app-toolbar',
	templateUrl: './toolbar.component.html',
})

export class ToolbarComponent implements OnChanges {
	@Input()
	id: number

	@Input()
	tasks: ITask[]

	assignedTasks: any
	inProgressTasks: any
	completedTasks: any

	constructor(private _uiService: UiService) {}

	ngOnChanges() {
		this.assignedTasks = this.tasks.filter(task => {
			const userTask = task.users.filter(u => u.user_id === this.id)[0]
			if (!userTask) { return false }

			return !userTask.completed && !userTask.taken_on
		})

		this.inProgressTasks = this.tasks.filter(task => {
			const userTask = task.users.filter(u => u.user_id === this.id)[0]
			if (!userTask) { return false }

			return !userTask.completed && userTask.taken_on
		})

		this.completedTasks = this.tasks.filter(task => {
			const userTask = task.users.filter(u => u.user_id === this.id)[0]
			if (!userTask) { return false }

			return userTask.completed && !userTask.accepted
		})
	}

	handleDevClick() {
		this._uiService.toggleDevtools()
	}

	toggleTheme() {
		this._uiService.toggleTheme()
	}

	toggleTasks(initialTabIndex) {
		this._uiService.toggleTasks(initialTabIndex)
	}
}
