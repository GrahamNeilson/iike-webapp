import { Component, Output, EventEmitter } from '@angular/core'

import { IdentityService } from '../../services'

@Component({
	selector: 'app-menu',
	template: `
    <button [routerLink]="['/']" (click)="handleClick()" mat-button appMenuButton>HOME</button>

    <div *ngIf="(identity | async).id">
      <button [routerLink]="['/users']" (click)="handleClick()" mat-button appMenuButton>USERS</button>
      <button [routerLink]="['/ideas']" (click)="handleClick()" mat-button appMenuButton>IDEAS</button>
      <button [routerLink]="['/abilities']" (click)="handleClick()" mat-button appMenuButton>ABILITIES</button>
      <button (click)="logout()" mat-button appMenuButton>LOGOUT</button>
    </div>
  `,
})
export class MenuComponent {
	identity: any
	@Output() menuItemClick: EventEmitter<any> = new EventEmitter()

	constructor(private _identityService: IdentityService) {
		this.identity = this._identityService.identity
	}

	handleClick() {
		this.menuItemClick.emit(null)
	}

	logout() {
		this._identityService.logout()
		this.handleClick()
	}
}
