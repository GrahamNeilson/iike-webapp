import 'hammerjs'

import { NgModule } from '@angular/core'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { BrowserModule } from '@angular/platform-browser'
import { HttpModule, Http, XHRBackend, RequestOptions, } from '@angular/http'
import { Router, RouterModule } from '@angular/router' // http interceptor

import { StoreModule, combineReducers } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { StoreLogMonitorModule, useLogMonitor } from '@ngrx/store-log-monitor'

/* Mine.
 */
import { SharedModule } from '../shared/shared.module'
import { routing } from './app.routes'

/* Components
 */
import { AppComponent } from './app.component'
import { HomeComponent } from './home/home.component'
import { DashboardComponent } from './home/dashboard.component'
import { GoogleOAuth20Component } from './googleOAuth2.0/googleOAuth2.0.component'
import { LayoutComponent, MenuComponent, ToolbarComponent } from './template'

/* Services
 */
import { HttpInterceptor } from '../services/http.interceptor'
import {
	UsersService,
	AbilitiesService,
	UiService,
	IdentityService,
	WindowService,
	AuthenticationService,
	NotificationService,
	AuthGuard,
	ConstantsService,
	IdeasService,
	IdeaCyclesService,
	TasksService,
	SocketService,
} from '../services'

/* Store
 */
import { UserEffects, IdeaEffects } from '../store/effects'

import {
	IdentityReducer,
	UiReducer,
	UsersReducer,
	UserReducer,
	AbilitiesReducer,
	SelectedAbilityReducer,
	IdeaReducer,
	IdeaCycleReducer,
	IdeaCycleTreeReducer,
	IdeaCycleTasksReducer,
} from '../store/reducers'

const reducers = {
	identity: IdentityReducer,
	ui: UiReducer,
	users: UsersReducer,
	user: UserReducer,
	abilities: AbilitiesReducer,
	selectedAbility: SelectedAbilityReducer,
	idea: IdeaReducer,
	ideaCycle: IdeaCycleReducer,
	ideaCycleTree: IdeaCycleTreeReducer,
	ideaCycleTasks: IdeaCycleTasksReducer,
}

// required to provide our http intercpetor, an extension of angular/http
// (this used to defined inline in the module providers section)
export function httpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions,
	router: Router, identityService: IdentityService) {
	return new HttpInterceptor(xhrBackend, requestOptions, router, identityService)
}

/* Configure StoreDevtoolsModule log monitor
*/
export function instrumentStore() {
	return {
		monitor: useLogMonitor({
			visible: true,
			position: 'right',
		}),
	}
}

@NgModule({
	declarations: [
		AppComponent,
		LayoutComponent,
		MenuComponent,
		ToolbarComponent,
		HomeComponent,
		GoogleOAuth20Component,
		DashboardComponent,
	],
	imports: [
		routing,
		BrowserModule,
		HttpModule,
		RouterModule,
		SharedModule,
		StoreModule.forRoot(reducers),
		EffectsModule.forRoot([
			UserEffects,
			IdeaEffects,
		]),
		StoreDevtoolsModule.instrument(instrumentStore),
		StoreLogMonitorModule,
		BrowserAnimationsModule,
	],
	providers: [
		AbilitiesService,
		UiService,
		UsersService,
		IdentityService,
		WindowService,
		AuthenticationService,
		NotificationService,
		AuthGuard,
		ConstantsService,
		IdeasService,
		IdeaCyclesService,
		TasksService,
		SocketService,
		{
			provide: Http,
			useFactory: httpFactory,
			deps: [XHRBackend, RequestOptions, Router, IdentityService],
		},

	],
	bootstrap: [AppComponent]
})
export class AppModule { }
