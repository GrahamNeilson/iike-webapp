import { Component, Input, Output, OnChanges, EventEmitter} from '@angular/core'
import { MatDialog } from '@angular/material'

import { ITask } from '../../../store/interfaces'
import { TasksService, NotificationService } from '../../../services'
import { ITaskUser } from '../../../store/interfaces/tasks.interface'
import { EditTaskDialogComponent } from './edit-task-dialog.component'

@Component({
	selector: 'app-task',
	styleUrls: ['./task.component.scss'],
	templateUrl: './task.component.html',
})
export class TaskComponent implements OnChanges {

	saving = false

	@Input()
	task: ITask

	@Input()
	currentUserId: number

	taskIsAssignedToIdentity: boolean
	taskIsOwnedByIdentity: boolean
	taskTakenOnByIdentity: boolean
	taskUsers: Array<ITaskUser>
	takenOnTaskUsers: Array<ITaskUser>

	constructor(
		public dialog: MatDialog,
		public tasksService: TasksService,
		public notificationService: NotificationService
	) {}

	ngOnChanges() {
		this.taskIsOwnedByIdentity = this.task.user_id === this.currentUserId

		this.taskIsAssignedToIdentity =
			this.task.users.filter(u => u.user_id === this.currentUserId).length > 0

		this.taskTakenOnByIdentity =
			this.task.users
				.filter(u => {
					return u.user_id === this.currentUserId && u.taken_on
				})
				.length > 0

		this.taskUsers = this.task.users
		this.takenOnTaskUsers = this.task.users.filter(u => u.taken_on)
	}

	handleAcceptTaskAsComplete(taskId: number): void {
		this.saving = true
		this.tasksService.acceptAsComplete(taskId).subscribe(
			() => this.notificationService.showSuccess('Task updated'),
			() => this.notificationService.showError('An error occurred'),
			() => this.saving = false
		)
	}

	handleRejectTaskAsComplete(taskId: number): void {
		this.saving = true
		this.tasksService.rejectAsComplete(taskId).subscribe(
			() => this.notificationService.showSuccess('Task updated'),
			() => this.notificationService.showError('An error occurred'),
			() => this.saving = false
		)
	}

	handleTakeOnTask(id: number): void {
		this.saving = true
		this.tasksService.takeOnTask(id).subscribe(
			() => this.notificationService.showSuccess('Task updated'),
			() => this.notificationService.showError('An error occurred'),
			() => this.saving = false
		)
	}

	handleRejectTask(id: number): void {
		this.saving = true
		this.tasksService.reject(id).subscribe(
			() => this.notificationService.showSuccess('Task updated'),
			() => this.notificationService.showError('An error occurred'),
			() => this.saving = false
		)
	}

	handleCompleteTask(id: number): void {
		this.saving = true
		this.tasksService.complete(id).subscribe(
			() => this.notificationService.showSuccess('Task updated'),
			() => this.notificationService.showError('An error occurred'),
			() => this.saving = false
		)
	}

	handleEdit(task) {
		const dialogRef = this.dialog.open(EditTaskDialogComponent, {
			width: '600px',
			disableClose: true,
		})

		dialogRef.componentInstance.subTitle = this.task.title
		dialogRef.componentInstance.title = 'Edit task'
		dialogRef.componentInstance.ideaCycleId = this.task.idea_cycle_id
		dialogRef.componentInstance.baseTask = this.task

		dialogRef.afterClosed().subscribe(data => {
			console.log('Task edit closed')
		})

	}
}
