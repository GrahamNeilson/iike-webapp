import { Component, Input, Output, EventEmitter } from '@angular/core'
import { MatDialog } from '@angular/material'

import { ITask } from '../../../store/interfaces'

@Component({
	selector: 'app-user-task-list',
	styles: [`
    .task + .task {
      margin-top: 8px
    }
  `],
	templateUrl: './user-task-list.component.html',
})
export class UserTaskListComponent {
	@Input()
	tasks: ITask[]

	@Input()
	currentUserId: number
}
