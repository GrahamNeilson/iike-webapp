import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core'
import { MatDialog } from '@angular/material'

import { Observable } from '../../../app/app.rx'
import { ITask } from '../../../store/interfaces'
import { TasksService, IdentityService, NotificationService } from '../../../services'

@Component({
	selector: 'app-user-tasks',
	styles: [`
    .task + .task {
      margin-top: 8px
    }
  `],
	templateUrl: './user-tasks.component.html',
})
export class UserTasksComponent implements OnChanges {
	@Input()
	tasks: ITask[]

	@Input()
	currentUserId: number

	@Input()
	tasksTabIndex: string

	assignedTasks: ITask[]
	inProgressTasks: ITask[]
	completedTasks: ITask[]
	acceptedAsCompleteTasks: ITask[]

	constructor(
		public dialog: MatDialog,
		public tasksService: TasksService,
		public identityService: IdentityService,
		public notificationService: NotificationService,
	) { }

	ngOnChanges() {
		this.assignedTasks = this.tasks.filter(task => {
			const userTask = task.users.filter(u => u.user_id === this.currentUserId)[0]
			if (!userTask) { return false }

			return !task.completed && !userTask.taken_on
		})

		this.inProgressTasks = this.tasks.filter(task => {
			const userTask = task.users.filter(u => u.user_id === this.currentUserId)[0]
			if (!userTask) { return false }

			return !task.completed && userTask.taken_on
		})

		this.completedTasks = this.tasks.filter(task => {
			const userTask = task.users.filter(u => u.user_id === this.currentUserId)[0]
			if (!userTask) { return false }

			return task.completed && !task.accepted
		})

		this.acceptedAsCompleteTasks = this.tasks.filter(task => {
			const userTask = task.users.filter(u => u.user_id === this.currentUserId)[0]
			if (!userTask) { return false }

			return task.accepted
		})
	}
}
