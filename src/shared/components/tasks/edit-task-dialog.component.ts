import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { MatDialogRef } from '@angular/material'

import { Observable } from '../../../app/app.rx'
import { ConstantsService, TasksService, NotificationService } from '../../../services'
import { IUser, IIdeaCycle, ITask, ITaskAsset, ITaskCategory } from '../../../store/interfaces'

@Component({
	selector: 'app-edit-task',
	styleUrls: ['./edit-task-dialog.component.scss'],
	templateUrl: './edit-task-dialog.component.html',
})
export class EditTaskDialogComponent implements OnInit {
	public title: string
	public subTitle: string
	public ideaCycleId: number
	public saving = false
	public taskCategories: Observable<ITaskCategory[]>
	public taskAssets: Observable<ITaskAsset[]>
	public baseTask: ITask = {
		id: 0,
		title: '',
		description: '',
		users: [],
	}

	taskForm: FormGroup
	titleMaxlength: number
	titleMinlength: number
	descriptionMaxlength: number
	descriptionMinlength: number

	constructor(
		public dialogRef: MatDialogRef<EditTaskDialogComponent>,
		private _formBuilder: FormBuilder,
		private _constantsService: ConstantsService,
		private _tasksService: TasksService,
		private _notificationService: NotificationService,
	) { }

	ngOnInit() {
		this.taskCategories =
			this._tasksService.taskCategories
				.publish()
				.refCount()

		this.taskAssets =
			this._tasksService.taskAssets
				.publish()
				.refCount()

		this.taskAssets.subscribe(assets => {
			assets
				.filter(a => a.isDefault)
				.map(defaultAsset => {
					this.taskForm.controls['asset'].setValue(this.baseTask.task_asset_id || defaultAsset.id)
				})
		})

		this.taskCategories.subscribe(categories => {
			categories
				.filter(c => c.isDefault)
				.map(defaultCategory => {
					this.taskForm.controls['category'].setValue(this.baseTask.task_category_id || defaultCategory.id)
				})
		})

		this.titleMaxlength = this._constantsService.task.title.maxlength
		this.titleMinlength = this._constantsService.task.title.minlength

		this.descriptionMaxlength = this._constantsService.task.description.maxlength
		this.descriptionMinlength = this._constantsService.task.description.minlength

		this.taskForm = this._formBuilder.group({
			title: [{
				value: this.baseTask.title,
				disabled: this.baseTask.is_default,
			}, Validators.compose([
					Validators.required,
					Validators.minLength(this.titleMinlength),
					Validators.maxLength(this.titleMaxlength)
				])
			],
			description: [this.baseTask.description, Validators.compose([
				Validators.minLength(this.descriptionMinlength),
				Validators.maxLength(this.descriptionMaxlength)
			])
			],
			category: [null, Validators.required],
			asset: [{value: null, disabled: this.baseTask.id}, Validators.required],
			ideaCycleId: [this.ideaCycleId],
			selectedUsers: [this.baseTask.users],
		})
	}

	handleSelectedUsersChange(users: IUser[]) {
		console.log('handle selected change')
		console.log(users)
		this.taskForm.controls['selectedUsers'].setValue(users)
	}

	handleSelectedUsersTouched() {
		this.taskForm.controls['selectedUsers'].markAsTouched()
	}

	save() {
		this.saving = true
		const task = Object.assign({}, this.taskForm.value, {
			selectedUsers: this.taskForm.controls['selectedUsers'].value.map(u => u.user_id)
		})

		const action = this.baseTask.id ? 'editTask' : 'createTask'
		const result = this.baseTask.id ? 'Task saved' : 'Task created'
		const verb = this.baseTask.id ? 'saving' : 'creating'

		if (this.baseTask.id) {
			task.id = this.baseTask.id
		}

		this._tasksService[action](task).subscribe(
			success => {
				this._notificationService.showSuccess(result)
				this.saving = false
				this.dialogRef.close()
			},
			err => {
				this._notificationService.showError(`There was a problem ${verb} your task`)
				this.saving = false
			}
		)
	}
}
