import { Component, Input, OnInit } from '@angular/core'
import { Router } from '@angular/router'

@Component({
	selector: 'app-asset-fab',
	templateUrl: 'asset-fab.component.html'
})
export class AssetFabComponent implements OnInit {

	@Input()
	task: any

	isFolder = false
	isDocument = false
	isSpreadsheet = false

	constructor(
		private router: Router
	) { }

	ngOnInit() {
		this.isFolder = this.task.task_asset_title === 'None'
		this.isDocument = this.task.task_asset_title === 'Document'
		this.isSpreadsheet = this.task.task_asset_title === 'Spreadsheet'
	}

	handleClick(e) {
		e.stopPropagation()

		if (this.isFolder) {
			this.router.navigate(['/ideas', this.task.idea_id])
		} else if (this.isSpreadsheet) {
			window.open(`https://docs.google.com/spreadsheets/d/${this.task.folder_id}`)
		} else if (this.isDocument) {
			window.open(`https://docs.google.com/document/d/${this.task.folder_id}`)
		}
	}
}
