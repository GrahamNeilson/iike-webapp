import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { FormControl } from '@angular/forms'
import { Observable } from '../../../app/app.rx'
import { UsersService } from '../../../services'
import { IUser } from '../../../store/interfaces'

@Component({
	selector: 'app-user-picker',
	styleUrls: ['./user-picker.component.scss'],
	templateUrl: './user-picker.component.html',
})
export class UserPickerComponent implements OnInit {
	@Input()
	selectedUsers: any[] = [] // task users at the moment

	@Input()
	title = 'Select users'

	@Output()
	usersChanged = new EventEmitter()

	@Output()
	touched = new EventEmitter()

	user = new FormControl()
	$users: Observable<any[]>
	$filteredUsers: Observable<IUser[]>

	constructor(
		public usersService: UsersService) { }

	ngOnInit() {
		this.$users =
			this.usersService.getUsers()
				.map(users => // we need this to tally with a task user, which has a user_display_name property
					users.map(user => Object.assign(user, {
							user_display_name: user.display_name,
							user_id: user.id,
						})
					)
				)

		this.$filteredUsers =
			Observable.combineLatest(
				this.user.valueChanges,
				this.$users
			).map(values => {
				const [currentInputValue, users] = values

				if (!currentInputValue) {
					return []
				}

				return users.filter(user => {
					return new RegExp(currentInputValue, 'gi').test(user.display_name)
				})
			})
	}

	add(user: any) {
		if (!user.user_id) {
			return
		}

		const existsAsSelection =
			this.selectedUsers
				.filter(selectedUser => {
					return selectedUser.user_id === user.user_id
				})
				.length > 0

		if (existsAsSelection) {
			return
		}

		this.selectedUsers.push(user)
		this.usersChanged.emit(this.selectedUsers)
		this.user.reset()
	}

	remove(user: any) {
		this.selectedUsers =
			this.selectedUsers.filter(selectedUser => selectedUser.user_id !== user.user_id)

		this.usersChanged.emit(this.selectedUsers)
	}

	displayFn(user: IUser): any {
		return user ? user.display_name : null
	}

	handleFocus() {
		this.touched.emit(true)
	}
}
