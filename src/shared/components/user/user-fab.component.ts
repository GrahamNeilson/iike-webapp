import { Component, Input, Optional } from '@angular/core'
import { Router } from '@angular/router'
import { MatDialog, MatDialogRef } from '@angular/material'

@Component({
	selector: 'app-user-fab',
	templateUrl: 'user-fab.component.html'
})
export class UserFabComponent {
	@Input()
	pictureUrl: string

	@Input()
	userId: number

	constructor(private router: Router) { }

	handleClick(e) {
		e.stopPropagation()
		this.router.navigate(['/users', this.userId])
	}
}
