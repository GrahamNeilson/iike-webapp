import { Component, Input } from '@angular/core'

@Component({
	selector: 'app-loading-indicator',
	styleUrls: ['./loading-indicator.component.css'],
	templateUrl: './loading-indicator.component.html',
})
export class LoadingIndicatorComponent {
	@Input()
	loading: boolean
}
