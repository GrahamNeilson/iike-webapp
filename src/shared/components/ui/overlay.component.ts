import {
	Component, Input,
	trigger,
	state,
	style,
	transition,
	animate,
} from '@angular/core'

@Component({
	animations: [
		trigger('fadeIn', [
			state('in', style({ opacity: 1 })),
			transition('void => *', [
				style({ opacity: 0 }),
				animate(300)
			]),
			transition('* => void', [
				animate(300, style({ opacity: 0 }))
			])
		])
	],
	selector: 'app-overlay',
	styleUrls: ['./overlay.component.css'],
	templateUrl: './overlay.component.html',
})
export class OverlayComponent {
	@Input()
	visible: boolean

	@Input()
	message: string
}
