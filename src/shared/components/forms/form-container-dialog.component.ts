import { Component, OnInit } from '@angular/core'
import { MatDialogRef } from '@angular/material'
import { Subject, Observable } from '../../../app/app.rx'

import {
	IFormSubject,
	IResult,
	ISaveFunction
} from '../../../store/interfaces'

@Component({
	// selector: 'app-form-container-dialog',
	styleUrls: ['./form-container-dialog.component.scss'],
	templateUrl: './form-container-dialog.component.html',
})
export class FormContainerDialogComponent implements OnInit {
	public subTitle: string
	public title: string
	public parentId: string

	// public saveFunction: ISaveFunction // needs work, so for now...
	public saveFunction: any
	public saving = false
	public canSave = false

	public formData: IFormSubject
	private formState = new Subject()

	constructor(public dialogRef: MatDialogRef<FormContainerDialogComponent>) { }

	ngOnInit() {
		this.formState.subscribe((formState: any) => {
			this.canSave = formState.canSave
			this.formData = formState.data
		})
	}

	handleFormStateChange(formState: IFormSubject) {
		this.formState.next(formState)
	}

	save() {
		this.saving = true

		this.saveFunction(this.formData).subscribe(
			result => {
				this.saving = false
				this.dialogRef.close({
					idea: this.formData,
					success: true
				})
			},
			err => {
				this.saving = false
				this.dialogRef.close()
			}
		)
	}
}
