import { Component, OnInit, Output, EventEmitter } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { Observable } from '../../../app/app.rx'
import { ConstantsService } from '../../../services'
import { IIdea } from '../../../store/interfaces'

@Component({
	selector: 'app-idea-form',
	templateUrl: 'idea-form.component.html'
})
export class IdeaFormComponent implements OnInit {
	@Output()
	formState = new EventEmitter()

	ideaForm: FormGroup

	titleMaxlength: number
	titleMinlength: number
	descriptionMaxlength: number
	descriptionMinlength: number

	constructor(
		private _formBuilder: FormBuilder,
		private _constantsService: ConstantsService) { }

	ngOnInit() {
		this.titleMaxlength = this._constantsService.idea.title.maxlength
		this.titleMinlength = this._constantsService.idea.title.minlength

		this.descriptionMaxlength = this._constantsService.idea.description.maxlength
		this.descriptionMinlength = this._constantsService.idea.description.minlength

		this.ideaForm = this._formBuilder.group({
			title: [null, Validators.compose([
				Validators.required,
				Validators.minLength(this.titleMinlength),
				Validators.maxLength(this.titleMaxlength)
			])
			],
			description: [null, Validators.compose([
				Validators.minLength(this.descriptionMinlength),
				Validators.maxLength(this.descriptionMaxlength)
			])
			],
		})

		Observable.combineLatest(
			this.ideaForm.valueChanges,
			this.ideaForm.statusChanges,
			(data, status) => {
				return {
					data,
					canSave: status === 'VALID',
				}
			})
			.debounceTime(100)
			.subscribe(formState => {
				this.formState.emit(formState)
			})
	}
}
