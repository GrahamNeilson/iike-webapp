import { Directive, ElementRef, OnInit } from '@angular/core'

/*
Styles a matButton directive for use in my menu.
Tbh, the menu should contextulaize. Todo, remove.
*/
@Directive({
	selector: '[appMenuButton]',
})
export class MenuButtonDirective implements OnInit {
	constructor(private _el: ElementRef) {
		_el.nativeElement.style.width = '100%'
		_el.nativeElement.style.borderRadius = '0'
	}

	ngOnInit(): void {
		/*
    Set style on child element created by mat-button directive
    The child span needs to be there, so we need the parent mat-button to have
    initialised.
    */
		if (this._el.nativeElement.children[0]) {
			this._el.nativeElement.children[0].style.textAlign = 'left'
			this._el.nativeElement.children[0].style.display = 'block'
		}
	}
}
