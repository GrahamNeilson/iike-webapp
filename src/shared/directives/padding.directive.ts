import { Directive, ElementRef } from '@angular/core'
/*
Apply our default padding to an element
*/
@Directive({
	selector: '[appPadding]',
})
export class PaddingDirective {
	constructor(private _el: ElementRef) {
		_el.nativeElement.style.padding = '8px'
	}
}
