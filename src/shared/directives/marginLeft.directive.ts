import { Directive, ElementRef } from '@angular/core'

/*
Apply our default margin left to an element
*/
@Directive({
	selector: '[appMarginLeft]',
})
export class MarginLeftDirective {
	constructor(private _el: ElementRef) {
		_el.nativeElement.style.marginLeft = '8px'
	}
}
