import { Directive, ElementRef } from '@angular/core'

/*
Apply padding left and right to the element
*/
@Directive({
	selector: '[appIconButton]',
})
export class IconButtonDirective {
	constructor(private _el: ElementRef) {
		_el.nativeElement.style.minWidth = 'initial'
		_el.nativeElement.style.paddingLeft = '8px'
		_el.nativeElement.style.paddingRight = '8px'
	}
}
