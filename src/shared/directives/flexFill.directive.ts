import { Directive, ElementRef } from '@angular/core'

@Directive({
	selector: '[appFlexFill]',
})
export class FlexFillDirective {
	constructor(private _el: ElementRef) {
		_el.nativeElement.style.flex = '1 1 auto'
	}
}
