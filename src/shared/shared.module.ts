import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MaterialModule } from '../material/material.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import {
	AssetFabComponent,
	LoadingIndicatorComponent,
	UserFabComponent,
	UserPickerComponent,
	OverlayComponent,
	UserTasksComponent,
	UserTaskListComponent,
	TaskComponent,
	EditTaskDialogComponent,
	FormContainerDialogComponent,
	IdeaFormComponent,
} from './components'

import {
	FlexFillDirective,
	MarginLeftDirective,
	MenuButtonDirective,
	PaddingDirective,
	IconButtonDirective,
} from './directives'

@NgModule({
	declarations: [
		AssetFabComponent,
		LoadingIndicatorComponent,
		UserFabComponent,
		UserPickerComponent,
		OverlayComponent,
		UserTasksComponent,
		UserTaskListComponent,
		TaskComponent,
		EditTaskDialogComponent,
		FormContainerDialogComponent,
		IdeaFormComponent,
		FlexFillDirective,
		MarginLeftDirective,
		MenuButtonDirective,
		PaddingDirective,
		IconButtonDirective,
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MaterialModule,
	],
	exports: [
		FormsModule,
		ReactiveFormsModule,
		MaterialModule,
		AssetFabComponent,
		LoadingIndicatorComponent,
		UserFabComponent,
		UserPickerComponent,
		OverlayComponent,
		UserTasksComponent,
		TaskComponent,
		UserTaskListComponent,
		EditTaskDialogComponent,
		FormContainerDialogComponent,
		IdeaFormComponent,
		FlexFillDirective,
		MarginLeftDirective,
		MenuButtonDirective,
		PaddingDirective,
		IconButtonDirective,
	],
	providers: [],
	entryComponents: [
		EditTaskDialogComponent,
		FormContainerDialogComponent,
	]
})
export class SharedModule { }
