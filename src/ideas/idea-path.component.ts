import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core'
import { Router } from '@angular/router'

import { Observable } from '../app/app.rx'
import { IdeasService } from '../services'
import { IIdea } from '../store/interfaces'

@Component({
	selector: 'app-idea-path',
	styleUrls: ['./idea-path.component.scss'],
	templateUrl: './idea-path.component.html',
})
export class IdeaPathComponent implements OnChanges {
	@Input()
	idea: IIdea

	public ideaPath$: Observable<IIdea[]>

	constructor(
		private ideasService: IdeasService,
		private router: Router,
	) { }

	ngOnChanges() {
		this.ideaPath$ = this.ideasService.ideaPath$(this.idea.id)
	}

	handleSelected(id: number): void {
		this.router.navigate(['/ideas', id])
	}
}
