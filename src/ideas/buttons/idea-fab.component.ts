import { Component, Input, Output, EventEmitter } from '@angular/core'
import { MatMenuTrigger, MatMenu, MatMenuItem, MatDialog } from '@angular/material'

import { IIdeaCycle } from '../../store/interfaces'
import {
	FormContainerDialogComponent
} from '../../shared/components/forms/form-container-dialog.component'
import { EditTaskDialogComponent } from '../../shared/components/tasks/edit-task-dialog.component'
import { Observable } from '../../app/app.rx'
import { IdeasService, NotificationService } from '../../services'


@Component({
	selector: 'app-idea-fab',
	styleUrls: ['idea-fab.component.scss'],
	templateUrl: 'idea-fab.component.html',
})
export class IdeaFabComponent {
	@Input()
	ideaCycle: IIdeaCycle

	constructor(
		public dialog: MatDialog,
		public ideaService: IdeasService,
		public notificationService: NotificationService,
	) { }

	handleIdeaClick() {
		const dialogRef = this.dialog.open(FormContainerDialogComponent, {
			width: '600px',
			disableClose: true,
		})

		dialogRef.componentInstance.title = 'Create idea'
		if (this.ideaCycle) {
			dialogRef.componentInstance.subTitle = this.ideaCycle.idea_title
			dialogRef.componentInstance.title = 'Create child idea'
		}

		dialogRef.componentInstance.saveFunction = (idea) => {
			if (this.ideaCycle) {
				idea.parentId = this.ideaCycle.idea_id
			}

			return this.ideaService.createIdea(idea)
		}

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				result.success ?
					this.notificationService.showSuccess(`Idea saved`) :
					this.notificationService.showError(`Error!`)
			}
		})
	}

	handleTaskClick() {
		const dialogRef = this.dialog.open(EditTaskDialogComponent, {
			width: '600px',
			disableClose: true,
		})

		dialogRef.componentInstance.title = 'Create task'
		dialogRef.componentInstance.subTitle = this.ideaCycle.idea_title
		dialogRef.componentInstance.ideaCycleId = this.ideaCycle.id

		dialogRef.afterClosed().subscribe(data => {
			console.log('Task edit closed')
		})
	}
}
