import { Component, Input, Output, EventEmitter } from '@angular/core'
import { Router } from '@angular/router'

import { Observable } from '../../app/app.rx'
import { IdeaCyclesService, TasksService, NotificationService} from '../../services/'
import { IIdeaCycle, IIdea, ITask } from '../../store/interfaces'

@Component({
	selector: 'app-idea-cycle-tree',
	styleUrls: ['./idea-cycle-tree.component.scss'],
	templateUrl: './idea-cycle-tree.component.html',
})
export class IdeaCycleTreeComponent {
	@Input()
	currentUserId: number

	ideaCycleTree$: Observable<IIdeaCycle[]>
	ideaCycleTasks$: Observable<ITask[]>

	public development$: Observable<any>
	public doing$: Observable<any>
	public developmentTasks$: Observable<ITask[]>
	public doingTasks$: Observable<ITask[]>

	constructor(
		private ideaCyclesService: IdeaCyclesService,
		private notificationService: NotificationService,
		private tasksService: TasksService,
		private router: Router,
	) {
		this.ideaCycleTree$ = this.ideaCyclesService.ideaCycleTree$
		this.ideaCycleTasks$ = this.ideaCyclesService.ideaCycleTasks$

		this.developmentTasks$ =
			this.ideaCycleTasks$.map(ideaCycleTasks =>
				ideaCycleTasks
					.filter(t => !t.completed)
					.filter((task: ITask) =>
						!task.users.filter(u => u.taken_on).length
					)
			)

		this.doingTasks$ =
			this.ideaCycleTasks$.map(ideaCycleTasks =>
				ideaCycleTasks
					.filter((task: ITask) =>
						(task.users.filter(u => u.taken_on).length || task.completed) && !task.accepted
					)
			)

		this.development$ =
			this.ideaCycleTree$.map(ideaCycles =>
				ideaCycles.slice(1).filter((ideaCycle: IIdeaCycle) =>
					!ideaCycle.in_progress
				))

		this.doing$ =
			this.ideaCycleTree$.map(ideaCycles =>
				ideaCycles.slice(1).filter((ideaCycle: IIdeaCycle) =>
					ideaCycle.in_progress
				))
	}

	handleSelectIdea(id: number): void {
		this.router.navigate(['/ideas', id])
	}
}
