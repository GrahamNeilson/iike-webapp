import { Component, Input, Output, EventEmitter } from '@angular/core'

import { IIdeaCycle } from '../../store/interfaces'

@Component({
	selector: 'app-idea-tile',
	styleUrls: ['./idea-tile.component.scss'],
	templateUrl: 'idea-tile.component.html',
})
export class IdeaTileComponent {
	@Input()
	ideaCycle: IIdeaCycle

	@Output()
	select = new EventEmitter()
}
