import {
	Component, Input, Output, EventEmitter,
	trigger,
	state,
	style,
	transition,
	animate,
} from '@angular/core'

import { IIdeaCycle, ITask } from '../../store/interfaces'

@Component({
	animations: [
		trigger('flyInOut', [
			state('in', style({ opacity: 1 })),
			transition('void => *', [
				style({ opacity: 0 }),
				animate(500)
			]),
			transition('* => void', [
				animate(500, style({ opacity: 0 }))
			])
		])
	],
	selector: 'app-idea-state',
	styles: [`
    .tile + .tile {
      margin-top: 8px
    }
  `],
	templateUrl: 'idea-state.component.html',
})
export class IdeaStateComponent {
	@Input()
	currentUserId: number

	@Input()
	ideaCycles: IIdeaCycle[]

	@Input()
	tasks: ITask[]

	@Input()
	title: string

	@Output()
	selectIdea = new EventEmitter()
}
