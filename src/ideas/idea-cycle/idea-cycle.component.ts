import { Component, Input, OnChanges, OnDestroy } from '@angular/core'

import { Observable } from '../../app/app.rx'
import { IIdeaCycle, ITask } from '../../store/interfaces'
import { IdeaCyclesService, NotificationService } from '../../services'

@Component({
	selector: 'app-idea-cycle',
	styleUrls: ['./idea-cycle.component.scss'],
	templateUrl: './idea-cycle.component.html',
})
export class IdeaCycleComponent implements OnChanges, OnDestroy {
	@Input()
	ideaCycle: IIdeaCycle

	@Input()
	currentUserId: number

	initialising = false

	ideaCycleTree$: Observable<IIdeaCycle[]>
	ideaCycleTasks$: Observable<ITask[]>

	constructor(
		private ideaCyclesService: IdeaCyclesService,
		private notificationService: NotificationService,
	) {
		this.ideaCycleTree$ = this.ideaCyclesService.ideaCycleTree$
		this.ideaCycleTasks$ = this.ideaCyclesService.ideaCycleTasks$
	}

	ngOnChanges() {
		if (this.ideaCycle && this.ideaCycle.active) {
			this.loadData()
		}
	}

	loadData() {
		this.ideaCyclesService.loadIdeaCycleTree(this.ideaCycle.id)
		this.ideaCyclesService.loadIdeaCycleTasks(this.ideaCycle.id)
	}

	createFreshCycle(ideaCycleId: number) {
		this.initialising = true
		this.ideaCyclesService.createFreshCycle(ideaCycleId)
			.subscribe(() => {
				this.initialising = false
				this.notificationService.showSuccess('New cycle created')
			})
	}

	createCycleFromPrevious(ideaCycleId: number) {
		this.initialising = true
		this.ideaCyclesService.createCycleFromPrevious(ideaCycleId)
			.subscribe(() => {
				this.initialising = false
				this.notificationService.showSuccess('New cycle created')
			})
	}

	reactivate(ideaCycleId: number) {
		this.ideaCyclesService.reactiveIdeaCycle(ideaCycleId)
			.subscribe(
				success => this.notificationService.showSuccess('Continuing on...'),
				() => this.notificationService.showSuccess('An error occurred'),
			)
	}

	ngOnDestroy() {
		this.ideaCyclesService.deleteIdeaCycleTasks()
		this.ideaCyclesService.deleteIdeaCycleTree()
	}
}
