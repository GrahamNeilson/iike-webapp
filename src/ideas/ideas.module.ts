import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { SharedModule } from '../shared/shared.module'
import { IdeasRoutingModule } from './ideas-routing.module'

import { IdeasComponent } from './ideas.component'
import { IdeaComponent } from './idea.component'
import { IdeaPathComponent } from './idea-path.component'
import { IdeaStateComponent } from './idea-cycle/idea-state.component'
import { IdeaTileComponent } from './idea-cycle/idea-tile.component'
import { IdeaCycleTreeComponent } from './idea-cycle/idea-cycle-tree.component'
import { IdeaCycleComponent } from './idea-cycle/idea-cycle.component'
import { IdeaPageComponent } from './idea-page.component'
import { IdeaFabComponent } from './buttons/idea-fab.component'

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		IdeasRoutingModule,
	],
	declarations: [
		IdeasComponent,
		IdeaComponent,
		IdeaPathComponent,
		IdeaStateComponent,
		IdeaTileComponent,
		IdeaCycleTreeComponent,
		IdeaCycleComponent,
		IdeaPageComponent,
		IdeaFabComponent,
	],
	exports: [],
	providers: [],
	entryComponents: [],
})
export class IdeasModule { }
