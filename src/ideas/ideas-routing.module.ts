import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { IdeasComponent } from './ideas.component'
import { IdeaPageComponent } from './idea-page.component'

@NgModule({
	imports: [RouterModule.forChild([
		{ path: '', component: IdeasComponent },
		{ path: ':id', component: IdeaPageComponent }
	])],
	exports: [RouterModule]
})
export class IdeasRoutingModule { }
