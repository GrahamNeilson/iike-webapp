import { Component, OnDestroy } from '@angular/core'
import { Observable } from '../app/app.rx'

import { IdeasService, IdentityService } from '../services'
import { IIdea, IIdeaCycle, IIdentity } from '../store/interfaces'

@Component({
	selector: 'app-ideas',
	templateUrl: './ideas.component.html',
})
export class IdeasComponent implements OnDestroy {
	public idea$: Observable<IIdea>
	public identity$: Observable<IIdentity>

	constructor(
		public _ideasService: IdeasService,
		public identityService: IdentityService
	) {
		this.idea$ = this._ideasService.idea$
		this.identity$ = this.identityService.identity

		// loading an idea loads the current cycle via idea effects / actions middleware
		this._ideasService.loadRootIdea()
	}

	ngOnDestroy() {
		this._ideasService.deleteIdea()
	}
}
