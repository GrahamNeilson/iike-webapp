import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { Observable } from '../app/app.rx'
import { IdeasService, IdentityService } from '../services'
import { IIdea } from '../store/interfaces'
import { IIdentity } from '../store/reducers/identity.reducer'

@Component({
	selector: 'app-idea-page',
	templateUrl: './idea-page.component.html',
})
export class IdeaPageComponent implements OnInit {
	public idea$: Observable<any>
	public identity$: Observable<IIdentity>

	constructor(
		private route: ActivatedRoute,
		private ideasService: IdeasService,
		private identityService: IdentityService
	) { }

	ngOnInit() {
		this.idea$ = this.ideasService.idea$
		this.identity$ = this.identityService.identity

		this.route.params.map(params =>
			this.ideasService.loadIdea(+params['id'])
		).subscribe()
	}
}
