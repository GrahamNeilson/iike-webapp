import { Component, Input, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'

import { Observable } from '../app/app.rx'
import { IdeaCyclesService } from '../services'
import { IIdea, IIdeaCycle } from '../store/interfaces'
import { IIdentity } from '../store/reducers/identity.reducer'

/**
 * this component takes the idea from both ideas (root idea) or idea page
 * it picks up the current idea cycle on the way through.
 */
@Component({
	selector: 'app-idea',
	styleUrls: ['./idea.component.scss'],
	templateUrl: './idea.component.html',
})
export class IdeaComponent implements OnDestroy {
	@Input()
	idea: IIdea

	@Input()
	identity: IIdentity

	public ideaCycle$: Observable<IIdeaCycle>

	constructor(private ideaCyclesService: IdeaCyclesService) {
		this.ideaCycle$ = this.ideaCyclesService.ideaCycle$
	}

	ngOnDestroy() {
		this.ideaCyclesService.deleteIdeaCycle()
	}
}
