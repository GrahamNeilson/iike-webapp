import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { MatDialog } from '@angular/material'

import { Observable } from '../app/app.rx'
import { IUser, IAbility } from '../store/interfaces'
import { UsersService } from '../services/users.service'
import { QuickEditDialogComponent } from './quick-edit-dialog.component'

@Component({
	selector: 'app-users',
	styleUrls: ['./users.component.scss'],
	templateUrl: './users.component.html',
})
export class UsersComponent implements OnInit {
	users: Observable<Array<IUser>>

	constructor(
		private usersService: UsersService,
		private router: Router,
		public dialog: MatDialog) { }

	ngOnInit() {
		this.users = this.usersService.users
		this.usersService.loadUsers()
	}

	handleSelected(id: number) {
		this.router.navigate(['/users', id])
	}

	handleQuickEditAbilities(userAndAbilitiesObject) {
		const { id, abilities } = userAndAbilitiesObject

		const dialogRef = this.dialog.open(QuickEditDialogComponent)
		dialogRef.componentInstance.selectedAbilities = abilities.map(a => a.title)

		dialogRef.afterClosed().subscribe(selectedAbilities => {
			if (selectedAbilities) {
				this.usersService.updateAbilities(id, selectedAbilities)
			}
		})
	}
}
