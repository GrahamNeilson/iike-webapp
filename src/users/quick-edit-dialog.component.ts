import { Component, OnInit, Input } from '@angular/core'
import { MatDialogRef } from '@angular/material'
import { FormControl } from '@angular/forms'

import { Observable } from '../app/app.rx'
import { AbilitiesService } from '../services'
import { IAbility } from '../store/interfaces'

/**
 * todo: matAutocompleteTrigger suport / documentation which should tidy up
 * selection of ability from panel.
 *
 * what this does at the moment is send back an array of strings
 */
@Component({
	selector: 'app-quick-edit-dialog',
	styleUrls: ['./quick-edit-dialog.scss'],
	templateUrl: './quick-edit-dialog.html',
})
export class QuickEditDialogComponent implements OnInit {
	ability = new FormControl()
	$abilities: Observable<IAbility[]>
	$filteredAbilities: Observable<IAbility[]>
	selectedAbilities: string[] = []

	constructor(
		public dialogRef: MatDialogRef<QuickEditDialogComponent>,
		public abilitiesService: AbilitiesService) { }

	ngOnInit() {
		this.$abilities =
			this.abilitiesService.loadAbilities()

		this.$filteredAbilities =
			Observable.combineLatest(
				this.ability.valueChanges,
				this.$abilities
			).map(values => {
				const [currentInputValue, abilities] = values

				if (!currentInputValue) {
					return []
				}

				return abilities.filter(ability => {
					return new RegExp(currentInputValue, 'gi').test(ability.title)
				})
			})
	}

	select(ability: FormControl) {
		if (!ability) {
			return
		}

		const value = ability.value

		const existsAsSelection =
			this.selectedAbilities.find(sa => sa.toLowerCase() === value.toLowerCase())

		if (existsAsSelection) {
			return
		}

		this.selectedAbilities.push(value)
		this.ability.reset()
	}

	remove(title) {
		this.selectedAbilities =
			this.selectedAbilities.filter(selectedAbility => selectedAbility !== title)
	}
}
