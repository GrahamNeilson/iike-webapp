import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { UsersComponent } from './users.component'
import { UserComponent } from './user.component'

@NgModule({
	imports: [RouterModule.forChild([
		{ path: '', component: UsersComponent },
		{ path: ':id', component: UserComponent }
	])],
	exports: [RouterModule]
})
export class UsersRoutingModule { }
