import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'

import { Observable } from '../app/app.rx'
import { IUser, IRequestedUser } from '../store/interfaces'
import { UsersService } from '../services'

@Component({
	selector: 'app-user',
	styleUrls: ['./user.component.css'],
	templateUrl: './user.component.html',
})
export class UserComponent implements OnInit {
	requestedUser: Observable<IRequestedUser>

	constructor(
		private usersService: UsersService,
		private route: ActivatedRoute,
		private router: Router) { }

	ngOnInit() {
		this.requestedUser = this.usersService.user

		this.route.params.subscribe(params => {
			const id = +params['id']
			this.usersService.requestUser(id)
		})
	}

	handleSelectedActivity(activityId: number) {
		this.router.navigate(['/activities', activityId])
	}
}
