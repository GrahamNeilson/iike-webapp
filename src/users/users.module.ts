import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { SharedModule } from '../shared/shared.module'
import { UsersRoutingModule } from './users-routing.module'
import { UsersComponent } from './users.component'
import { UserComponent } from './user.component'
import { UserSummaryComponent } from './user-summary.component'
import { UserListComponent } from './user-list.component'
import { QuickEditDialogComponent } from './quick-edit-dialog.component'

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		UsersRoutingModule,
	],
	declarations: [
		UserComponent,
		UsersComponent,
		UserListComponent,
		UserSummaryComponent,
		QuickEditDialogComponent,
	],
	exports: [],
	providers: [],
	entryComponents: [
		QuickEditDialogComponent
	]
})
export class UsersModule { }
