import { Component, Input } from '@angular/core'
import { IUser } from '../store/interfaces'

@Component({
	selector: 'app-user-summary',
	template: `
    <!--<app-loading-indicator [loading]="user.loading"></app-loading-indicator>-->

    <div *ngIf="!user.loading">
      {{user.first_name}} {{user.surname}}
      <br />
      {{user.email}}
    </div>
  `,
})

export class UserSummaryComponent {
	@Input()
	user: IUser
}
