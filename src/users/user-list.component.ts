import { Component, Input, Output, EventEmitter } from '@angular/core'

import { IUser } from '../store/interfaces'

@Component({
	selector: 'app-user-list',
	templateUrl: 'user-list.component.html',
})
export class UserListComponent {
	@Input()
	users: IUser[]

	@Output()
	selected = new EventEmitter()

	@Output()
	quickEditAbilities = new EventEmitter()
}
